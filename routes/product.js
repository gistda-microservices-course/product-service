const express = require('express');
const router = express.Router();
const passportJWT = require("../middlewares/passport-jwt");
const Product = require("../models/product");

// create product
/* localhost:5000/api/v1/product/ */
router.post('/', [passportJWT.isLogin] , async function(req, res, next) {
    const { product_id, product_name, product_price } = req.body;

    const newProduct = await Product.create({
        product_id: product_id,
        product_name: product_name,
        product_price: product_price,
        user_id: req.user.user_id,
    });

    return res.status(201).json({
        message: "เพิ่มสินค้าสำเร็จ",
        data: newProduct
    });

});

// get all product
/* localhost:5000/api/v1/product/ */
router.get('/' , async function(req, res, next) {
   
    const products = await Product.find({}).sort({ _id: "desc" });

    return res.status(200).json({
        data: products
    });

});


module.exports = router;
